
const express = require('express')
const app = express()
const http = require('http').Server(app)
const fp = require('path')

//redisClient.init()
// шаблонизаторы
const hbs = require('express-hbs')


// сервисы для разработки
const morgan = require('morgan') // логгер

function relative(path) {
  return fp.join(__dirname, path)
}

//Инициализация сервисов
initViewEngine()
initStaticFiles()


http.listen(8081, function () {
    console.log('server started')
})
app.use('/', (req, res) => {
    res.render('gameScreenView', {})
})


function initStaticFiles() {
  app.use(express.static('public'))
}

function initViewEngine() {
  var helpers = require('handlebars-helpers')({
    handlebars: hbs
  })
  hbs.registerHelper('json', function (context) {
    return JSON.stringify(context)
  })

  app.engine('hbs', hbs.express4({
    //partialsDir: relative('/views/partials'),
    defaultLayout: relative('/views/layout/mainLayout.hbs'),
    layoutsDir: relative('views/layout')
  }))

  app.set('view engine', 'hbs')
  app.set('views', __dirname + '/views')
}
