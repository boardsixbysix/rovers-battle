var gulp = require('gulp')
var browserify = require('browserify')
var source = require('vinyl-source-stream')
var babel = require('babelify')
var uglify = require('gulp-uglify')
var buffer = require('vinyl-buffer')


gulp.task('start', function () {
    exec('pm2 start app.js', function (err, stdout, stderr) {
        console.log(stdout)
        console.log(stderr)
    });
})

gulp.task('stop', function () {
    exec('pm2 stop app.js', function (err, stdout, stderr) {
        console.log(stdout)
        console.log(stderr)
    });
})

gulp.task('rs', function () {
    exec('pm2 restart app.js', function (err, stdout, stderr) {
        console.log(stdout)
        console.log(stderr)
    });
})

gulp.task('browserifyGame', function () {
    return browserify({
        entries: ['public/src/start.js']
    })
        .transform(babel, { presets: ["es2017"] })
        .bundle()
        .on('error', function (err) {
            console.log(err.toString())
            this.emit('end')
        })
        .pipe(source('bundleGame.js'))

        // Передаем имя файла, который получим на выходе, vinyl-source-stream
        .pipe(gulp.dest('./public/build/'))
})

gulp.task('compress', function (cb) {
    pump([
        gulp.src('./public/build/'),
        uglify(),
        gulp.dest('build')
    ],
        cb
    )
})


gulp.task('build', ['browserify', 'start'])