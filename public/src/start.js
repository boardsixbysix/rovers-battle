const level = require('./game/states/level');
const boot = require('./game/states/boot');
const load = require('./game/states/load')
const credits = require('./game/states/credits')

$(document).ready(() => {

    var canvas = $('#game-area')[0]
    window.game = new Phaser.Game(window.innerWidth, window.innerHeight, Phaser.CANVAS, 'game-area')

    window.game.state.add('level', level)
    window.game.state.add('load', load)
    window.game.state.add('boot', boot)
    window.game.state.add('credits', credits)

    window.game.state.start('boot')

},
    (error) => {
        console.log(error)
    })
