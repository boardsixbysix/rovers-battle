var Promise = require('bluebird')

var style = { font: "32px Arial", fill: "#ffff44", align: "center" }
var styleButtonRestart = { font: "15px Arial", fill: "#00ff44", align: "center" }

module.exports = {

    successResult: null,

    preload: function () {
        this.successResult = game.successResult

    },

    create: function () {

        var resultText = ""
        if (this.successResult) {
            game.stage.backgroundColor = '#cccccc'
            resultText = "YOU ARE WIN!"
        } else {
            game.stage.backgroundColor = '#000000'
            resultText = "GAME OVER"
        }

        var resTextLabel = game.add.text(game.camera.view.centerX, game.camera.view.centerY, resultText, style)

        var buttonRestart = game.add.text(game.camera.view.centerX, game.camera.view.centerY + 50, 'New game', styleButtonRestart)
        buttonRestart.inputEnabled = true
        buttonRestart.input.enableDrag()
        buttonRestart.events.onInputDown.add(() => {
            console.log('input restart')
            game.state.start('level')
        }, this)

        buttonRestart.events.onInputOver.add(item => {
            item.fill = "#ffff44";
        });
        buttonRestart.events.onInputOut.add(item => {
            item.fill = "#ff0044";
        });
    }
}
