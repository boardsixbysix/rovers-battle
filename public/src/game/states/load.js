var Promise = require('bluebird')

module.exports = {

  count: 0,

  loadingLabel: function () {

    this.loading = game.add.sprite(game.world.centerX, game.world.centerY - 20, 'loading')
    this.loading.anchor.setTo(0.5, 0.5)
    game.load.setPreloadSprite(this.loading)

  },

  preload: function () {
    this.loadingLabel()

  },

  create: function () {

    game.state.start('level')
  }



}
