
const Rover = require('../scene/rover')
const mapEngine = require('../scene/mapEngine')
const repairDetailGenerator = require('../scene/repairDetailGenerator')
const gasGenerator = require('../scene/gasGenerator')
const systemWeather = require('../scene/systemWeather')
const Sensor = require('../scene/sensor')
const ChoiceView = require('../scene/choiceView')
const Helper = require('../helper.js');


var style = { font: "16px Arial", fill: "#ff0044" }
var styleProgressLevel = { font: "24px Verdana", fill: "#696969", boundsAlignH: "center", boundsAlignV: "middle" }
var styleForward = { font: "26px Arial Black", fill: "#00cccc", boundsAlignH: "center", boundsAlignV: "middle", stroke: '#6a5aCD', strokeThickness:6 }

module.exports = {
  cursors: null,
  rover: null,
  backSprite: null,

  slotId: null,
  team: -1,

  hpLine: null,
  gasLine: null,
  researchLine: null,
  researchBack: null,
  currentSpeed: 0,
  readyToResearchText: null,
  //readyToResearch: false,
  //processResearch: false,
  maxResearchPoints: 0,

  researchProgress: 0,
  researches: null,

  progressLevel: 0,
  progressLevelLabel: null,

  sensor: null,

  preload: function () {
    //game.load.image('rover', 'assets/image/rover.png')
    game.load.spritesheet('rover', 'assets/rover_move.png', 60, 80, 12)

    game.load.image('repair', 'assets/image/repair.png')

    game.load.image('red_sand', 'assets/image/red_sand.png')

    game.load.physics('physicsData', 'assets/collisionMask.json')
    game.load.json('repairResp', 'assets/repairResp.json')
    game.load.json('trees', 'assets/trees.json')

    for (var index = 1; index <= 3; index++) {
      game.load.image('cloud_dark' + index, `assets/image/clouds/cloud_dark${index}.png`)
    }

    game.load.image('rain', 'assets/image/rain.png')
    game.load.image('map_01', 'assets/image/decorations/map_01.png')
    game.load.image('low_charge', 'assets/image/low_charge.png')
    game.load.image('finish_back', 'assets/image/finish_back.png')
    game.load.image('rover_floor', 'assets/image/rover_floor.png')
    game.load.image('research_icon', 'assets/image/research_icon.png')
    game.load.image('research_bar', 'assets/image/hud/research_bar.png')

    game.load.image('progress-bar', 'assets/image/hud/progress-bar.png')
    game.load.image('blue-bar', 'assets/image/hud/blue-bar.png')
    game.load.image('red-bar', 'assets/image/hud/red-bar.png')
    game.load.image('yelow-bar', 'assets/image/hud/yelow-bar.png')
    game.load.image('energia', 'assets/image/hud/energia.png')
    game.load.image('hp', 'assets/image/hud/hp.png')
    game.load.image('research_icon_panel', 'assets/image/hud/research_icon_panel.png')
    game.load.image('full_screen', 'assets/image/hud/full_screen.png')

    game.load.json('researches', 'assets/researches.json')

    game.load.audio('rain', 'assets/audio/rain.mp3')

    game.load.audio('rain-thunder', 'assets/audio/rain-thunder.mp3')
    game.load.audio('bensound-newdawn', 'assets/audio/bensound-newdawn.mp3')
    game.load.audio('bensound-scifi', 'assets/audio/bensound-scifi.mp3')

  },

  create: function () {

    game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT

    game.time.events.events = []
    this.finished = false
    this.maxResearchPoints = 0
    systemWeather.reset()
    repairDetailGenerator.reset()
    //game.renderer.roundPixels = false;
    game.forceSingleUpdate = true;
    game.camera.roundPx = true

    //this.researchProgress = 0
    this.progressLevel = 0

    game.stage.disableVisibilityChange = true;
    game.forceSingleUpdate = true;
    // Инициализация графики сцены
    // Фон
    //game.stage.backgroundColor = '#909090'
    game.world.setBounds(0, 0, 2499, 2499)

    game.physics.startSystem(Phaser.Physics.P2JS)

    this.back = game.add.sprite(0, 0, 'map_01')
    this.back.width = 2500
    this.back.height = 2500

    var fake = game.add.sprite(0, 0, 'map_01')
    game.physics.p2.enable(fake)
    fake.width = 1
    fake.height = 1
    fake.body.static = true
    fake.body.clearShapes()
    fake.body.loadPolygon('physicsData', 'map_01_mount_1')
    fake.body.loadPolygon('physicsData', 'map_01_mount_2')
    fake.body.loadPolygon('physicsData', 'map_01_mount_3')
    fake.body.loadPolygon('physicsData', 'map_01_mount_4')
    fake.body.loadPolygon('physicsData', 'map_01_mount_5')
    fake.body.loadPolygon('physicsData', 'map_01_lake')
    fake.body.loadPolygon('physicsData', 'map_01_river')
    fake.body.loadPolygon('physicsData', 'map_01_zik_basa')
    fake.body.loadPolygon('physicsData', 'map_01_zik_lake')
    fake.body.loadPolygon('physicsData', 'map_01_zik_left_stone')

    key2 = game.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR)
    keyNitro = game.input.keyboard.addKey(Phaser.KeyCode.SHIFT)
    keyNitro.onDown.add(() => {
      this.rover.bonusSpeed += 80
      this.rover._resСonsuming += 0.18
    })
    keyNitro.onUp.add(() => {
      this.rover.bonusSpeed -= 80
      this.rover._resСonsuming -= 0.18
    })
    //key2 = game.input.keyboard.addKey(Phaser.KeyCode.R)
    key2.onDown.add(() => {
      this.sensor.tryStartResearch()
    })

    this.cursors = game.input.keyboard.createCursorKeys()

    this.sensor = new Sensor()
    this.rover = new Rover()
    this.rover.sensor = this.sensor
    this.rover.init()
    repairDetailGenerator.generateBaseForRover(this.rover)
    // gasGenerator.generateBaseForRover(this.rover)

    //this._initEnvironment()
    this._initResearches()
    this._initUi()
    this._initTimers()

    this.startMusic = game.add.audio('bensound-newdawn');
    this.startMusic.volume = 0.2
    this.startMusic.play()

    this.rains = []
    this.rains.push(game.add.audio('rain'))

    //this.rains.push(game.add.audio('rain3'))

    this.rains.push(game.add.audio('rain-thunder'))
    systemWeather.init(this.rover, this.rains, () => {
      this.startMusic.stop()
      this.actionMusic = game.add.audio('bensound-scifi');
      this.actionMusic.volume = 0.2
      this.actionMusic.play()
    })




  },

  render: function () {
    //game.debug.text(game.time.fps, 2, 14, '#00ff00')
  },

  angSpeed: 0,
  update: function () {

    this._checkFinish()

    if (this.hpLine)
      this.hpLine.width = this.rover.hp * 1.3

    if (this.finished) return
    systemWeather.pulse()
    this.hpLine.bringToTop()
    this.gasLine.bringToTop()
    this.sensor.updateUI()
        this.progressLevelBar.bringToTop()

    this.progressLevelLabel.bringToTop()
    this.rover.floor.bringToTop()
    this.fullScreenButton.bringToTop()

    this.sensor.processResearch((current) => {
      this._addPoints(current)
      this._showDiscovery(current)
    }, this.rover)

    if (this.rover.disablePower) return

    if (this.cursors.up.isDown) {
      if (this.sensor.isProcessResearch) {
        this.sensor._resetReadyToResearch()
      }
      this.currentSpeed = 60 + this.rover.bonusSpeed - this.rover.deceleration
      this.rover.сonsuming()
      this.rover.moveForward(this.currentSpeed, true)
    } else if (this.cursors.down.isDown) {
      if (this.sensor.isProcessResearch) {
        this.sensor._resetReadyToResearch()
      }
      this.currentSpeed = -30

      this.rover.moveForward(this.currentSpeed)
    } else {
      if (this.currentSpeed != 0) {
        if (this.currentSpeed > 0) this.currentSpeed -= 1
        else this.currentSpeed += 1
        this.rover.moveForward(this.currentSpeed)
      } else {
        if (!this.sensor.isProcessResearch && this.sensor.current && !this.sensor.readyToResearch) {
          this.sensor.setReadyToResearch()
        }
        this.rover.moveForward(0)
      }
    }

    if (this.cursors.left.isDown) {
      if (this.sensor.isProcessResearch) {
        this.sensor._resetReadyToResearch()
      }
      this.rover.сonsuming()
      this.angSpeed = 14 + this.rover.bonusSpeed / 4
      this.rover.rotateLeft(this.angSpeed, true)
      this.left = true
      this.right = false
    }
    else if (this.cursors.right.isDown) {
      if (this.sensor.isProcessResearch) {
        this.sensor._resetReadyToResearch()
      }
      this.rover.сonsuming()
      this.angSpeed = 14 + this.rover.bonusSpeed / 4
      this.rover.rotateRight(this.angSpeed, true)
      this.right = true
      this.left = false
    } else {
      if (this.angSpeed > 0) {
        this.angSpeed -= 1
        if (this.left) {
          this.rover.rotateLeft(this.angSpeed)
        } else if (this.right) this.rover.rotateRight(this.angSpeed)
      } else {
        this.rover.body.setZeroRotation()
      }
    }

    //if(this.rover.body.angularVelocity != 0){
    //  if(this.rover.body.angularVelocity > 0) this.rover.body.angularVelocity -= 0.2
    ///    else this.rover.body.angularVelocity += 0.2
    // }

    //game.physics.arcade.velocityFromRotation(this.rover.rotation, this.currentSpeed, this.rover.body.velocity)
  },

  hpMult: 1,
  _initTimers: function () {
    //таймеры
    /* game.time.events.loop(100, () => {
       this.rover.hp -= 0.05 * this.hpMult
       this.hpLine.width = this.rover.hp
     }, this)*/

    game.time.events.loop(500, () => {
      this.rover.accumulation()
      this.gasLine.width = this.rover.gas * 1.3
    })
  },

  _initUi: function () {

    //Дополнительные элементы
    //полоска хп
    var bmdHp = game.add.bitmapData(100, 10);
    bmdHp.ctx.beginPath();
    bmdHp.ctx.rect(0, 0, 100, 10);

    bmdHp.ctx.fillStyle = '#ff0000'
    bmdHp.ctx.fill();
    var startY = game.camera.view.height / 4
    this.hpLine = game.add.sprite(60, startY, 'red-bar')
    //this.hpLine.scale.setTo(1.3, 1.3)
    this.hpLine.fixedToCamera = true

    this.hpLine_border = game.add.sprite(60, startY, 'progress-bar')
    //hpLine_border.anchor.setTo(0.5, 0.5)
    this.hpLine_border.fixedToCamera = true

    this.hpIcon = game.add.sprite(30, startY, 'hp')
    //hpLine_border.anchor.setTo(0.5, 0.5)
    this.hpIcon.fixedToCamera = true

    //this.hpLine_border_lowHp = game.add.sprite(60, startY, 'progress-bar-low-hp')
    //hpLine_border.anchor.setTo(0.5, 0.5)
    //this.hpLine_border_lowHp.fixedToCamera = true
    //полоска бензина
    var bmdGas = game.add.bitmapData(100, 10);
    bmdGas.ctx.beginPath();
    bmdGas.ctx.rect(0, 0, 100, 10);

    bmdGas.ctx.fillStyle = '#0000ff'
    bmdGas.ctx.fill();
    this.gasLine = game.add.sprite(60, startY + 30, 'yelow-bar')
    //this.gasLine.anchor.setTo(0.5, 0.5)
    this.gasLine.fixedToCamera = true
    //this.gasLine.scale.setTo(1.3, 1.3)

    var gasLine_border = game.add.sprite(60, startY + 30, 'progress-bar')
    //gasLine_border.anchor.setTo(0.5, 0.5)
    gasLine_border.fixedToCamera = true

    this.gasIcon = game.add.sprite(30, startY + 30, 'energia')
    //hpLine_border.anchor.setTo(0.5, 0.5)
    this.gasIcon.fixedToCamera = true

    //Текст результата
    this.progressLevelLabel = game.add.text(0, 10, this._buildPointsLabel(), styleProgressLevel)
    this.progressLevelLabel.setTextBounds(0, 25, game.camera.view.width, 25)
    this.progressLevelLabel.fixedToCamera = true
    this.progressLevelLabel.fontWeight = 'bold'

    //Текст результата
    this.progressLevelBar = game.add.sprite(game.camera.view.width / 2 - this.progressLevelLabel.width / 2, 10, 'research_bar')
    this.progressLevelBar.scale.setTo(0.3, 0.3)
    this.progressLevelBar.alignIn(new Phaser.Rectangle(0, 30, game.camera.view.width, 30), Phaser.CENTER);
    this.progressLevelBar.fixedToCamera = true


    //Скрытые тексты

    this.finishLabel = game.add.text(0, 10, 'You completely researched the Planet', { font: "36px Arial Black", fill: "#FFD700", boundsAlignH: "center", boundsAlignV: "middle", stroke: '#6A5ACD', strokeThickness: 6 })
    this.finishLabel.fixedToCamera = true
    this.finishLabel.setTextBounds(0, game.camera.view.centerY / 3, game.camera.view.width, game.camera.view.centerY / 3)
    this.finishLabel.fontWeight = 'bold'
    this.finishLabel.alpha = 0

    var bHeight = game.camera.view.centerY / 3 + this.finishLabel.height + 20

    this.buttonRestart = game.add.text(0, 10, 'New game', { font: "30px Arial Black", fill: "#0000CD", boundsAlignH: "center", boundsAlignV: "middle", stroke: '#6A5ACD', strokeThickness: 6 })
    this.buttonRestart.setTextBounds(0, bHeight, game.camera.view.width, bHeight)
    this.buttonRestart.fixedToCamera = true
    this.buttonRestart.fontWeight = 'bold'
    this.buttonRestart.alpha = 0

    this.fullScreenButton = game.add.sprite(game.camera.view.width - 120, game.camera.view.height - 50, 'full_screen');
    this.fullScreenButton.scale.setTo(0.5, 0.5)
    this.fullScreenButton.fixedToCamera = true

    this.fullScreenButton.inputEnabled = true
    this.fullScreenButton.input.enableDrag()
    this.fullScreenButton.events.onInputDown.add(() => {
      if (game.scale.isFullScreen) {
        game.scale.stopFullScreen();
      }
      else {
        game.scale.startFullScreen(false);
      }})

  },

  _initResearches: function () {

    //Загрузка исследований

    var researchBase = {
      paid: 0,
      extra: 0,
      sprite: null,
      current: false,
      finished: false
    }
    var contentsJs = game.cache.getJSON('researches')

    contentsJs.forEach(function (element) {
      Object.assign(element, researchBase)
    }, this);

    var researches = contentsJs

    maxResearchPoints = 0
    researches.forEach(research => {
      this.maxResearchPoints += research.cost
    })
    //this.maxResearchPoints = 0

    researches.forEach(function (research) {
      var bmdHp = game.add.bitmapData(10, 10);
      bmdHp.ctx.beginPath();
      bmdHp.ctx.rect(0, 0, 10, 10);

      bmdHp.ctx.fillStyle = '#cc0010'
      bmdHp.ctx.fill();
      var rSprite = game.add.sprite(research.x, research.y, bmdHp)
      rSprite.width = 1
      rSprite.height = 1
      rSprite.anchor.setTo(0.5, 0.5)

      game.physics.p2.enable(rSprite)
      rSprite.body.setCircle(research.radius)
      rSprite.body.immovable = true
      rSprite.body.data.shapes[0].sensor = true
      rSprite.body.number = research.id

      rSprite.body.onBeginContact.add(this._hitSprite, { research: research, level: this })
      rSprite.body.onEndContact.add((body, bodyB, shapeA, shapeB, equation) => {
        if (!Helper.isRoverBody(body)) return
        this.sensor.removeCurrent()
        this.sensor._resetReadyToResearch()
      }, this.sensor)

      research.sprite = rSprite

      research.icon = game.add.sprite(research.x, research.y, 'research_icon')
      // icon.alpha = 0.7
      research.icon.anchor.setTo(0.5, 0.5)

    }, this)
  },

  _hitEnviroment: function (body, bodyB, shapeA, shapeB, equation) {
    if (!Helper.isRoverBody(body)) return
    var rover = this.rover
    var level = this.level
    rover.destruct()
    level.hpLine.width = rover.hp
  },

  _hitSprite: function (body, bodyB, shapeA, shapeB, equation) {
    if (!Helper.isRoverBody(body)) return
    res = this.research
    if (res.finished) return
    this.level.sensor.changeCurrent(res)
    console.log('hit!')
  },

  tween: null,

  _checkFinish: function () {
    if (this.finished) return

    if (this.maxResearchPoints <= this.progressLevel) {
      if (this.startMusic)
        this.startMusic.stop()
      if (this.actionMusic)
        this.actionMusic.stop()
      game.successResult = true
      this.finished = true
      systemWeather.reset()
      repairDetailGenerator.reset()

      game.time.events.add(Phaser.Timer.SECOND * 2, () => {

        var backFinish = game.add.sprite(0, 0, 'finish_back')
        //backFinish.anchor.setTo(0, 0)
        backFinish.fixedToCamera = true
        backFinish.width = game.camera.view.width
        backFinish.height = game.camera.view.height

        this.finishLabel.alpha = 1
        this.finishLabel.bringToTop()

        this.buttonRestart.alpha = 1
        this.buttonRestart.inputEnabled = true
        this.buttonRestart.input.enableDrag()
        this.buttonRestart.events.onInputDown.add(() => {
          console.log('input restart')
          game.paused = false
          game.state.start('level')
        }, this)

        this.buttonRestart.events.onInputOver.add(item => {
          item.fill = "#ffff44"
        })
        this.buttonRestart.events.onInputOut.add(item => {
          item.fill = "#0000CD"
        })

      }, this)
      return
    }

    if (this.rover.hp <= 0) {
      if (this.startMusic)
        this.startMusic.stop()
      if (this.actionMusic)
        this.actionMusic.stop()

      this.rover.sprite.animations.play('death', 1, false)
      this.finished = true
      game.successResult = false
      console.log('player is dead ' + this.rover.hp)
      setTimeout(() => {
        game.state.start('credits')

      }, 3000)
      return
    }
  },

  _addPoints: function (res) {

    var poits = 0

    res.icon.destroy()

    var timer = game.time.events.repeat(100, res.cost, () => {
      this.progressLevel += 1
      this.progressLevelLabel.text = this._buildPointsLabel()
    }, this)

    if (this.rover.bonusFortune != 0) {
      var magic = game.rnd.integerInRange(0, 100)
      if (magic < this.rover.bonusFortune) {
        res.extra += res.cost
      }
    }

    if (res.id == 3) {
      res.extra = 0
    }

    if (res.name == 'lake' || res.name == 'cave' || res.name == 'right_skeletons') {
      if (!this.sand_disable) this.sand_disable = 0
      this.sand_disable++
      if (this.sand_disable == 3) {
        systemWeather.sand.destroy()
      }
    }

    setTimeout(() => {
      if (res.extra == 0) return

      var extraCounter = 0
      this.progressLevelLabel.alpha = 0
      var extraLabel = game.add.text(500, 40, "BONUS " + this.progressLevel, { font: "34px Arial", fill: "#FFFFE0" })
      extraLabel.setTextBounds(0, 50, game.camera.view.width, 20)
      extraLabel.fixedToCamera = true
      game.time.events.repeat(100, res.extra, () => {
        extraCounter++
        this.progressLevel += 1
        extraLabel.text = 'BONUS ' + this.progressLevel
        if (extraCounter == res.extra - 1) {
          setTimeout(() => {
            extraLabel.destroy()
            this.progressLevelLabel.alpha = 1
            this.progressLevelLabel.text = this._buildPointsLabel()
          }, 1000)
        }
      }, this)
    }, 100 * res.cost + 1000)
  },

  _showDiscovery: function (res) {
    var disText = game.add.text(0, 20, res.text, styleForward)
    disText.wordWrap = true
    disText.wordWrapWidth = game.camera.view.width * 0.6
    disText.align = "center"
    disText.setTextBounds(0, game.camera.view.height / 4, game.camera.view.width, 20)
    disText.fixedToCamera = true
    disText.fontSize = 30

    var bonusText = game.add.text(0, 30, "🡩 " + res.bonus, { font: "40px Arial Black", fill: "#00FF00", boundsAlignH: "center", boundsAlignV: "middle", stroke: '#6A5ACD', strokeThickness: 6 })
    bonusText.setTextBounds(0, game.camera.view.height / 2, game.camera.view.width, 20)
    bonusText.fixedToCamera = true

    this.rover.addBonus(res.name)

    setTimeout(() => {
      bonusText.destroy()
    }, 4000)

    setTimeout(() => {
      disText.destroy()
    }, 15000)
  },

  _buildPointsLabel: function () {
    return '' + this.progressLevel + " / " + this.maxResearchPoints
  },

  _initEnvironment: function () {

    var mapCoords = game.cache.getJSON('trees')

    mapCoords.forEach(function (entity) {


      var spr1 = game.add.sprite(entity.x * 93, entity.y * 93, entity.texture)
      spr1.anchor.setTo(0.5, 0.5)
      game.physics.p2.enable(spr1)
      spr1.body.setCircle(10)
      spr1.body.static = true
      //spr1.body.onBeginContact.add(this._hitEnviroment, { rover: this.rover, level: this })

    }, this);
  }
}
