const JSON5 = require('json5')
var fs = require('fs')

var style = { font: "26px Arial Black", fill: "#00cccc", boundsAlignH: "center", boundsAlignV: "middle", stroke: '#6A5ACD', strokeThickness: 3 }
const systemWeather = require('./systemWeather.js');

module.exports = class Sensor {
    constructor() {
        this.readyToResearch = false
        this.isProcessResearch = false
        this.researchProgress = 0
        this.current = null

        //view
        var startY = game.camera.view.height / 4
        //полоска исследования
        var bmdResearch = game.add.bitmapData(100, 15);
        bmdResearch.ctx.beginPath();
        bmdResearch.ctx.rect(0, 0, 100, 15);

        bmdResearch.ctx.fillStyle = '#0800cc'
        bmdResearch.ctx.fill();
        this.researchLine = game.add.sprite(60, startY + 60, 'blue-bar')
        //this.researchLine.anchor.setTo(0, 0)
        this.researchLine.fixedToCamera = true
        this.researchLine.width = 0

        //полоска исследования - фон
        var bmdResearchBack = game.add.bitmapData(100, 15);
        bmdResearchBack.ctx.beginPath();
        bmdResearchBack.ctx.rect(0, 0, 100, 15);

        bmdResearchBack.ctx.fillStyle = '#F5DEB3'
        bmdResearchBack.ctx.fill();
        this.researchBack = game.add.sprite(60, startY + 60, 'progress-bar')
        //this.researchBack.anchor.setTo(0, 0)
        this.researchBack.fixedToCamera = true

        this.researchIcon = game.add.sprite(30, startY + 60, 'research_icon_panel')
        //this.researchBack.anchor.setTo(0, 0)
        this.researchIcon.fixedToCamera = true

        //Текст готовности к ииследованиям
        this.readyToResearchText = game.add.text(0, 0, "", style)
        this.readyToResearchText.setTextBounds(0, game.camera.view.centerY, game.camera.view.width, game.camera.view.centerY)
        this.readyToResearchText.fixedToCamera = true

    }

    processResearch(callback, rover) {
        if (!this.isProcessResearch) return

        rover.sprite.animations.play('research', 3, false)

        if ((this.current.cost - this.current.cost * rover.bonusResearch) <= this.current.paid) {
            if (!this.current.finished) {
                this.current.finished = true
                this._resetReadyToResearch()
                rover.dif++
                if (rover.dif == 3) {
                    systemWeather._addTimerRain()

                    var bonusText = game.add.text(0, 100, "The thunderstorm approaches. I have to manage to investigate the southeast!", { font: "40px Arial Black", fill: "#FF0000", boundsAlignH: "center", boundsAlignV: "middle", stroke: '#6A5ACD', strokeThickness: 6 })
                    bonusText.setTextBounds(0, game.camera.view.height / 2, game.camera.view.width, 20)
                    bonusText.wordWrap = true
                    bonusText.wordWrapWidth = game.camera.view.width * 0.6
                    bonusText.align = "center"
                    bonusText.fixedToCamera = true

                    setTimeout(() => {
                        bonusText.destroy()
                    }, 8000)
                }
                if (rover.dif == 4) {
                    systemWeather._addTimerRain()
                    systemWeather._addTimerRain()

                    var bonusText = game.add.text(0, 100, "Thunderstorm already here! I have to go to the last purpose: to the central temple", { font: "40px Arial Black", fill: "#FF0000", boundsAlignH: "center", boundsAlignV: "middle", stroke: '#6A5ACD', strokeThickness: 6 })
                    bonusText.setTextBounds(0, game.camera.view.height / 2, game.camera.view.width, 20)
                    bonusText.wordWrap = true
                    bonusText.wordWrapWidth = game.camera.view.width * 0.6
                    bonusText.align = "center"
                    bonusText.fixedToCamera = true

                    setTimeout(() => {
                        bonusText.destroy()
                    }, 8000)

                }
                callback(this.current)
                this.current = null

            }
        } else {
            if (this.current.name == 'zik') {
                if (rover.dif == 4) {
                    this.current.paid += 0.08
                }
            }
            this.current.paid += 0.01
            this.researchLine.width = (130 * this.current.paid) / (this.current.cost - this.current.cost * rover.bonusResearch)
            this.readyToResearchText.text = ""
        }
    }

    tryStartResearch() {
        if (this.readyToResearch) {
            this.isProcessResearch = true
        }
    }

    changeCurrent(research) {
        if (res.cost <= res.paid) {
            return
        }
        this.researchLine.width = (130 * res.paid) / res.cost

        this.current = res
        this.setReadyToResearch()
    }

    removeCurrent() {
        this.current = null
    }

    setReadyToResearch() {
        this.isProcessResearch = false
        this.readyToResearch = true
        this.readyToResearchText.text = 'Press SPACEBAR to research'
        this.readyToResearchText.alpha = 0

        this.tween = game.add.tween(this.readyToResearchText).to({ alpha: 1 }, 2000, "Linear", true)
        this.tween.repeat(10, 1000)
    }

    _resetReadyToResearch() {
        this.readyToResearch = false
        this.isProcessResearch = false
        this.readyToResearchText.text = ""
        this.researchLine.width = 0
    }

    updateUI() {
        this.researchBack.bringToTop()
        this.researchLine.bringToTop()
        this.readyToResearchText.bringToTop()
    }



}