module.exports = class ChoiceView {


    constructor() {
        this._choicePanel = []

        this._active = 0
    }


    init(choice, callbackOnMakeChoice) {
        this._model = choice

        this.START_TEXT_POS = game.camera.view.centerY
        if (window.screen.availWidth > 1400) {
            this.FONT_SIZE = 28
            this.SPRITE_BACKGROUND_HEIGHT = 40 * 3
        }
        else {
            this.FONT_SIZE = 26
        }
        this._callbackOnMakeChoice = callbackOnMakeChoice
    }

    pushRemark(remark) {
        this._model.push(remark)
    }

    execute(command, args) {
        console.log('Выполнение команды вьюшки - отображения выбора диалога')
        switch (command) {
            case "init":

                for (var index = 0; index < this._model.choice.length; index++) {
                    var choiceItem = this._model.choice[index]
                    var text = game.add.text(0, this.START_TEXT_POS + index * this.FONT_SIZE, choiceItem.text, { font: `${this.FONT_SIZE}px Verdana`, fill: "#ffffff", align: "center", boundsAlignH: "center", boundsAlignV: "middle" })
                    text.fixedToCamera = true
                    text.setTextBounds(0, this.FONT_SIZE, game.camera.view.width, this.FONT_SIZE)
                    text.id = choiceItem.id
                    text.alpha = 0

                    text.index = index

                    this._choicePanel.push(text)

                    if (index == 0) {
                        text.fill = "#00fcfc"
                    }

                }

                break

            case "show":

                for (var index = 0; index < this._choicePanel.length; index++) {
                    var choiceItem = this._choicePanel[index]
                    choiceItem.inputEnabled = true
                    choiceItem.input.enableDrag()
                    choiceItem.events.onInputOver.add(this.over, this);
                    choiceItem.events.onInputOut.add(this.out, this);
                    choiceItem.events.onInputDown.add(this.down, this)
                    choiceItem.alpha = 1
                    choiceItem.bringToTop()
                }

                this.handleArrowKeys()
                break

            case "hide":

                for (var index = 0; index < this._choicePanel.length; index++) {
                    var choiceItem = this._choicePanel[index]
                    choiceItem.destroy()
                }

                break
            default:
                break
        }
    }

    handleArrowKeys() {
        Mousetrap.bindGlobal('up', () => {
            if (this._active == 0) return
            var oldChoice = this._choicePanel[this._active]
            this.out(oldChoice)
            this._active--
            var newChoice = this._choicePanel[this._active]
            this.over(newChoice)
        })

        Mousetrap.bindGlobal('down', () => {
            if (this._active == this._choicePanel.length - 1) return
            var oldChoice = this._choicePanel[this._active]
            this.out(oldChoice)
            this._active++
            var newChoice = this._choicePanel[this._active]
            this.over(newChoice)
        })

        Mousetrap.bindGlobal('space', () => {
            var currentChoice = this._choicePanel[this._active]
            this._callbackOnMakeChoice(currentChoice.id)
        })
    }

    down(item) {
        var id = item.id
        item.fill = "#000000";
        this._callbackOnMakeChoice(id)
    }

    over(item) {
        this._active = this._choicePanel.indexOf(item)
        item.fill = "#00fcfc";
    }

    out(item) {
        item.fill = "#ffffff";
    }

    destroy() {
    }
}
