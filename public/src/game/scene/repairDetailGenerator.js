const Helper = require('../helper.js');


module.exports = {
    generateBaseForRover: function (rover, events) {
        mapSegments = game.cache.getJSON('repairResp')
        this.disposed = false
        for (var index = 0; index < 5; index++) {
            var smallDetails = new SmallDetails()
            var indexSg = game.rnd.integerInRange(0, mapSegments.length - 1)
            var seg = mapSegments[indexSg]
            smallDetails.addAt(seg.x, seg.y)

            smallDetails.details.body.onBeginContact.add(hitRepairDetails, { details: smallDetails, rover: rover })
        }

        var self = this
        this.timer = (function resDetails(time) {
            return game.time.events.add(time, () => {
                if (self.disposed) return

                var smallDetails = new SmallDetails()
                if (rover.bonusFortune != 0) {
                    var magic = game.rnd.integerInRange(0, 100)
                    var minDist = 99999
                    var minIndex = 0
                    for (var index = 0; index < mapSegments.length - 1; index++) {
                        var dist = distance(rover.sprite.x, rover.sprite.y, mapSegments[index].x, mapSegments[index].y)
                        if (dist < minDist) {
                            minDist = dist
                            minIndex = index
                        }
                    }
                    
                    smallDetails.addAt(mapSegments[minIndex].x, mapSegments[minIndex].y)
                    smallDetails.details.body.onBeginContact.add(hitRepairDetails, { details: smallDetails, rover: rover })
                    
                } else {
                    var indexSg = game.rnd.integerInRange(0, mapSegments.length - 1)
                    var seg = mapSegments[indexSg]
                    smallDetails.addAt(seg.x, seg.y)
                }
                smallDetails.details.body.onBeginContact.add(hitRepairDetails, { details: smallDetails, rover: rover })
                resDetails(Phaser.Timer.SECOND * (15 + rover.dif))
            })
        })(Phaser.Timer.SECOND * 20)

    },
    reset: function () {
        game.time.events.remove(this.timer)
        this.disposed = true
    }
}


 

 var distance = function (x1, y1, x2, y2) {

        var dx = x1 - x2;
        var dy = y1 - y2;

        return Math.sqrt(dx * dx + dy * dy);

    }
var hitRepairDetails = function (body, bodyB, shapeA, shapeB, equation) {
    if (!Helper.isRoverBody(body)) return

    detail = this.details
    rover = this.rover

    console.log('hit details')
    if ((rover.hp + detail.power) > 100) {
        rover.hp = 100
    } else {
        rover.hp += detail.power
    }

    detail.dispose()
}

class SmallDetails {
    constructor() {
        this.power = game.rnd.integerInRange(40, 60)
        this.sprite = null
    }

    get details() {
        return this.sprite
    }

    dispose() {
        this.sprite.destroy()
    }

    addAt(x, y) {
        var bmdDetails = game.add.bitmapData(10, 10);
        bmdDetails.ctx.beginPath();
        bmdDetails.ctx.rect(0, 0, 20, 20);

        bmdDetails.ctx.fillStyle = '#0000cf'
        bmdDetails.ctx.fill();
        this.sprite = game.add.sprite(x, y, 'repair')
        this.sprite.anchor.setTo(0.5, 0.5)

        game.physics.p2.enable(this.sprite)
        this.sprite.body.setCircle(20)
        this.sprite.body.data.shapes[0].sensor = true
    }
}