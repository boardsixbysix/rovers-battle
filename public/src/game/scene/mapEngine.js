module.exports = {
    rovers: [],

    addRover: function (rover) {
        this.rovers.push(rover)
    },

    updateRovers: function (roversData) {
        this.rovers.forEach(rover => {
            var data = roversData.find(roverData => roverData.id == rover.id)
            if (rover.currentSpeed >= 0) {
                rover.sprite.body.angularVelocity = data.angularVelocity
                game.physics.arcade.velocityFromRotation(rover.sprite.rotation, data.currentSpeed, rover.sprite.body.velocity)
            }
        })
    }
}