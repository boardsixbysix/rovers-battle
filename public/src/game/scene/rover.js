module.exports = class Rover {
    constructor() {

        this.hp = 100
        this.gas = 100
        this.points = 0

        this._blockAccumulationEnergy
        this._accumulationEnergy = 1.4
        this._resСonsuming = 0.04

        this.sprite = null

        this.dif = 0

        this.bonusSpeed = 0
        this.bonusImmunRain = 0
        this.bonusResearch = 0
        this.ImmunEq = 0
        this.bonusFortune = 0

        this.deceleration = 0
    }

    addBonus(name) {
        switch (name) {
            case 'right_skeletons':
                this.bonusSpeed = 50
                break;

            case 'left_skeletons':
                this.bonusImmunRain = 0.5
                break;

            case 'lake':
                this.bonusResearch = 0.5
                break;

            case 'zik':
                this.ImmunEq = 0.6
                break;

            case 'cave':
                this.bonusFortune = 50
                break;

            default:
                break;
        }
    }

    get body() {
        return this.sprite.body
    }

    blockAccumulation() {
        this._blockAccumulationEnergy = true
    }

    unBlockAccumulation() {
        this._blockAccumulationEnergy = false
    }

    moveForward(speed, isActive) {
        if (isActive) {
            if (this.gas <= 6) {
                //Выключение питания
                this._disablePower()
                this.body.rotateLeft(0)
                this.body.moveForward(0)
                this.body.rotateRight(0)
                return
            }
        }
        if (speed != 0)
            this.sprite.animations.play('walk', 6, false)

        this.body.moveForward(speed)

        this.floor.x = this.sprite.x
        this.floor.y = this.sprite.y

        this.floor.angle = this.sprite.angle
    }

    rotateLeft(speed, isActive) {
        if (isActive) {
            if (this.gas <= 6) {
                //Выключение питания
                this._disablePower()
                this.body.rotateLeft(0)
                this.body.moveForward(0)
                this.body.rotateRight(0)
                return
            }
        }
        this.body.rotateLeft(speed)

        this.floor.x = this.sprite.x
        this.floor.y = this.sprite.y
        this.floor.angle = this.sprite.angle
    }

    rotateRight(speed, isActive) {
        if (isActive) {
            if (this.gas <= 6) {
                //Выключение питания
                this._disablePower()
                this.body.rotateLeft(0)
                this.body.moveForward(0)
                this.body.rotateRight(0)
                return
            }
        }
        this.body.rotateRight(speed)

        this.floor.x = this.sprite.x
        this.floor.y = this.sprite.y
        this.floor.angle = this.sprite.angle
    }

    destruct(power, isRain) {
        //console.log('destruct invoke is rain ' + isRain+' power:'+ power)
        //console.log(this.hp)
        if (isRain && this.disablePower) {
            if (this.gas >= 100) return

            if ((this.gas + 0.2) > 100) {
                this.gas = 100
            } else {
                this.gas += 0.2
            }
            return
        }
        if (isRain) power -= this.bonusImmunRain

        var damage = power
        if ((this.hp - damage) < 0) {
            this.hp = 0
        } else {
            this.hp -= damage
        }
    }

    accumulation() {
        if (this.gas >= 20 && this.disablePower) {
            if (this.mask) this.mask.destroy()
            this.disablePower = false
        }
        if (this.gas >= 100 || this._blockAccumulationEnergy) return

        if ((this.gas + this._accumulationEnergy) > 100) {
            this.gas = 100
        } else {
            this.gas += this._accumulationEnergy
        }

    }

    сonsuming() {
        if (this.gas <= 0) return      
        this.gas -= (this._resСonsuming)
    }

    init() {
        var color = '#0000ff'

        var bmd = game.add.bitmapData(ROVER_SIZE.height, ROVER_SIZE.width);
        bmd.ctx.beginPath()
        bmd.ctx.rect(0, 0, ROVER_SIZE.height, ROVER_SIZE.width)

        bmd.ctx.fillStyle = color
        bmd.ctx.fill()

        this.floor = game.add.sprite(0, 0, 'rover_floor')
        this.floor.anchor.set(0.5)
        this.floor.alpha = 0

        var coords = [
            { x: 1110, y: 1010 },
            { x: 200, y: 410 },
            { x: 1300, y: 1930 }
        ]

        var coors = coords[game.rnd.integerInRange(0, coords.length - 1)]
        this.sprite = game.add.sprite(coors.x, coors.y, 'rover')
        this.sprite.anchor.setTo(0.5, 0.5)

        //this.sprite.shadow = shadow
        //this.rover.animations.add('move', ['tank1', 'tank2', 'tank3', 'tank4', 'tank5', 'tank6'], 20, true)

        game.physics.p2.enable(this.sprite)
        this.sprite.body.immovable = false
        this.sprite.damping = 0.5

        //this.sprite.shadow.x = 800
        //this.sprite.shadow.y = 2100

        game.camera.follow(this.sprite, Phaser.Camera.FOLLOW_LOCKON, 0.05, 0.05)
        game.camera.deadzone = new Phaser.Rectangle(300, 150, 500, 300);
        game.camera.focusOnXY(0, 0)

        var walk = this.sprite.animations.add('walk', [4, 5, 6])
        var research = this.sprite.animations.add('research', [1, 2, 3, 4])
        var finishAction = this.sprite.animations.add('death', [8])
    }

    _disablePower() {
        if (this.disablePower) return
        this.mask = game.add.sprite(0, 0, 'low_charge');
        this.mask.anchor.set(0.5);
        this.mask.x = this.sprite.x
        this.mask.y = this.sprite.y
        this.mask.angle = this.sprite.angle

        this.disablePower = true
    }
}

var ROVER_SIZE = {
    width: 80,
    height: 60
}