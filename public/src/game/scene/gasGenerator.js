module.exports.generateBaseForRover = function (rover) {
    for (var index = 0; index < 3; index++) {
        var randomSegmentIndex = game.rnd.integerInRange(0, mapSegments.length - 1)
        var segment = mapSegments[randomSegmentIndex]
        if(segment.busy) continue
        segment.busy = true
        var smallGas = new SmallGas()
        smallGas.addAt(game.rnd.integerInRange(segment.x, segment.x + segment.h), game.rnd.integerInRange(segment.y, segment.y + segment.w))

        smallGas.gas.body.onBeginContact.add(hitGas, { gas: smallGas, rover: rover })
    }
}

var hitGas = function (body, bodyB, shapeA, shapeB, equation) {
    gas = this.gas
    rover = this.rover

    console.log('hit gas')
    if ((rover.gas + gas.power) > 100) {
        rover.gas = 100
    } else {
        rover.gas += gas.power
    }

    gas.dispose()
}

var mapSegments = [
    { x: 600, y: 500, h: 300, w: 300 },
    { x: 300, y: 600, h: 100, w: 200 },
    { x: 900, y: 500, h: 100, w: 300 },
    { x: 500, y: 500, h: 100, w: 300 },
    { x: 1000, y: 500, h: 100, w: 300 },
    { x: 1200, y: 1000, h: 100, w: 300 },
    { x: 700, y: 700, h: 100, w: 300 },
]

class SmallGas {
    constructor() {
        this.power = 25
        this.sprite = null
    }

    get gas() {
        return this.sprite
    }

    dispose() {
        this.sprite.destroy()
    }

    addAt(x, y) {
        var bmdDetails = game.add.bitmapData(10, 10);
        bmdDetails.ctx.beginPath();
        bmdDetails.ctx.rect(0, 0, 15, 15)

        bmdDetails.ctx.fillStyle = '#cccccc'
        bmdDetails.ctx.fill();
        this.sprite = game.add.sprite(x, y, 'gas')
        this.sprite.anchor.setTo(0.5, 0.5)

        game.physics.p2.enable(this.sprite)
        this.sprite.body.setCircle(15)
        this.sprite.body.data.shapes[0].sensor = true
    }
}