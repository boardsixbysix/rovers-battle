const Helper = require('../helper')
module.exports = {
    //clear ; idle ; cloud; darkCloud ; Earthquake
    currentState: "",
    sRover: null,
    lastState: "",

    clouds: [],
    rains: null,
    timersRain: [],
    countDarkCloudOnRover: 0,
    countCloudOnRover: 0,

    sand: null,

    get countClouds() {
        return this.clouds.filter(cloud => !cloud.isDestroy).length
    },

    reset: function () {
        //if (this.cloud) this.cloud.destroy()
        this.currentState = "clear"
        this.rain.destroy()
        this.contactOnCloud = false
        this.contactOnDarkCloud = false
        this.countDarkCloudOnRover = 0
        timersRain.forEach(function (timer) {

        }, this);
    },

    init: function (rover, audio, callbackOnFirstCloudContact) {


        this.timerRain = game.time.events.loop(200, () => {
            if(this.countDarkCloudOnRover != 0)
            this.sRover.destruct(0.62, true)
        }, this);

        this.countDarkCloudOnRover = 0

        this.rains = audio
        this.sRover = rover
        this.currentState = 'clear'
        this.callbackOnFirstCloudContact = callbackOnFirstCloudContact
        this.isFirstContact = true
        var self = this
        this.clouds.length = 0
        this.cloudCollisionGroup = game.physics.p2.createCollisionGroup()

        this.rain = game.add.sprite(0, 0, 'rain')
        this.rain.fixedToCamera = true
        this.rain.width = game.camera.view.width
        this.rain.height = game.camera.view.height
        this.rain.alpha = 0

        this.sand = game.add.sprite(60, 1600, 'red_sand')
        this.sand.alpha = 0.5
        this.sand.width = 1200
        this.sand.height = 700
        //this.sand.anchor.set(0ю5, 0)
        game.physics.p2.enable(this.sand)
        this.sand.body.data.shapes.forEach(function (element) {
            element.sensor = true
        }, this)
        this.sand.body.onBeginContact.add((body) => {
            if (!Helper.isRoverBody(body)) return
            this.timerSand = game.time.events.loop(200, () => {
                this.sRover.destruct(0.6 - 0.2 * this.sRover.dif)
            })

            this.sRover.deceleration = 30

        }, this)
        this.sand.body.onEndContact.add((body) => {
            if (!Helper.isRoverBody(body)) return
            this.sRover.deceleration = 0
            game.time.events.remove(this.timerSand)
            this.timerSand = null
        }, this)

        this._addTimerRain()

        //Создание статических туч
        this.createDarkCloud(1850, 1070, 3, 0)

        var nextUpdate = 10

        this._timer2 = (function asyncUpdate(nextUpdate) {
            return game.time.events.add(nextUpdate * Phaser.Timer.SECOND, () => {

                var magic = game.rnd.realInRange(1, 100)
                var change = 25 + 10 * self.sRover.dif
                //var change = 100 + 10 * self.sRover.dif
                if (self.sRover.hp > 70)
                    var power = game.rnd.realInRange(15 + 5 * self.sRover.dif, 56 + 2 * self.sRover.dif)
                else {
                    var power = game.rnd.realInRange(15 + 5 * self.sRover.dif, self.sRover.hp - 10)
                }

                if (self.sRover.hp < 10 && self.sRover.dif == 4 || self.sRover.hp < 10 && self.sRover.dif == 3) {
                    power = game.rnd.realInRange(0, 1)
                    nextUpdate = 15
                    change = 30
                } else if (self.sRover.hp < 20) {
                    nextUpdate = 10
                    change = 10
                    if (self.sRover.dif == 4 || self.sRover.dif == 3) {
                        power = game.rnd.realInRange(1, 2)
                    } else
                        power = game.rnd.realInRange(1, 4 + self.sRover.hp)
                } else if (self.sRover.hp < 45) {
                    if (self.sRover.dif == 3) {
                        power = game.rnd.realInRange(1, 8)
                    } else power = game.rnd.realInRange(26, self.sRover.hp - 5)
                    if (self.sRover.dif == 4) {
                        power = game.rnd.realInRange(1, 10)
                    }
                    nextUpdate = 15
                    change = 30

                } else {
                    nextUpdate = game.rnd.realInRange(13 - 2 * self.sRover.dif, 27 - 3 * self.sRover.dif)
                    // nextUpdate = game.rnd.realInRange(1 - 2*self.sRover.dif, 3- 3 * self.sRover.dif)
                }

                if (magic < change) {
                    self.createEarthquake(power)
                }
                //console.log(new Date().toUTCString() + ' Earthquake created next update for ' + nextUpdate)
                asyncUpdate(nextUpdate)
            })
        })(nextUpdate)

    },

    reset: function () {

        game.time.events.remove(this._timer2)
        game.time.events.remove(this._timer)
        game.time.events.remove(this.timerRain)
        this.timerRain = null
    },

    rain: null,
    pulse: function () {
        this.clouds.forEach(function (cloudEntity) {
            let cloud = cloudEntity.cloud
            if (cloud.x > 2800) {
                //this.reset()
                cloud.destroy()
                cloud.isDestroy = true
                cloudEntity.shadow.destroy()
                return
            }
            cloudEntity.shadow.body.moveRight(cloud.speed)

            cloudEntity.cloud.x = cloudEntity.shadow.x - 50;
            cloudEntity.cloud.y = cloudEntity.shadow.y - 50;

            if (this.contactOnDarkCloud && cloud.type == 'dark') {
                //console.log('rain pulse destroy');
                //this.sRover.destruct(0.062, true)
            }
        }, this);

        if (this.rain)
            this.rain.bringToTop()

    },
    createDarkCloud: function (x, y, n, speed) {


        if (x == undefined) {
            if (this.sRover.dif == 4) {
                x = -800, y = game.rnd.integerInRange(640, 2000)
            } else {
                x = -800, y = game.rnd.integerInRange(-100, 2300)
            }
        }
        if (n == undefined) n = game.rnd.integerInRange(1, 3)
        if (this.sRover.dif == 1 && n == 3) {
            if (!this.countBigDarkCloudOn1) this.countBigDarkCloudOn1 = 0
            if (this.countBigDarkCloudOn1 == 3) {
                n = game.rnd.integerInRange(1, 2)
            } else
                this.countBigDarkCloudOn1++
        }
        if (this.sRover.dif == 2 && n == 3) {
            if (!this.countBigDarkCloudOn2) this.countBigDarkCloudOn2 = 0
            if (this.countBigDarkCloudOn2 == 4) {
                n = game.rnd.integerInRange(1, 2)
            } else
                this.countBigDarkCloudOn2++
        }
        if (this.sRover.dif == 3 && n == 3) {
            if (!this.countBigDarkCloudOn3) this.countBigDarkCloudOn3 = 0
            if (this.countBigDarkCloudOn3 == 6) {
                n = game.rnd.integerInRange(1, 2)
            } else
                this.countBigDarkCloudOn3++
        }
        var cloud = game.add.sprite(x, y, 'cloud_dark' + n)
        cloud.anchor.set(0.5);
        //cloud.x = -cloud.width
        cloud.type = 'dark'
        cloud.alpha = 0.6
        //cloud.width = widthCloud
        //this.cloud.fixedToCamera = true
        //this.cloud.height = 4200

        if (speed == undefined) speed = game.rnd.integerInRange(10, 40)
        cloud.speed = speed

        var shadow = game.add.sprite(cloud.x, cloud.y, cloud.key);
        shadow.anchor.set(0.5);
        shadow.tint = 0x000000;
        shadow.alpha = 0.6;

        game.physics.p2.enable(shadow)
        shadow.body.clearShapes()
        shadow.body.loadPolygon('physicsData', shadow.key)
        shadow.body.data.shapes.forEach(function (element) {
            element.sensor = true
        }, this)

        this.clouds.push({ cloud: cloud, shadow: shadow })

        shadow.body.onBeginContact.add(this._hitDarkCloudToRover, { typeCloud: 'dark', rover: this.sRover, system: this })
        shadow.body.onEndContact.add(this._endContactOnDarkCloud, this)
    },
    createEarthquake: function (power) {
        var rumbleOffset = 10;

        // we need to move according to the camera's current position
        if (game.camera.view.x < 10) {
            var tempXOffset = 20
            game.camera.view.x += 20
        }
        if (game.camera.view.x > 2490) {
            var tempXOffset = -20
            game.camera.view.x -= 20
        }

        var properties = {
            x: game.camera.view.x - rumbleOffset
        };

        // we make it a relly fast movement
        var duration = 100;
        // because it will repeat
        var repeat = 4;
        // we use bounce in-out to soften it a little bit
        var ease = Phaser.Easing.Bounce.InOut;
        var autoStart = false;
        // a little delay because we will run it indefinitely
        var delay = 1000;
        // we want to go back to the original position
        var yoyo = true;

        var quake = game.add.tween(game.camera.view)
            .to(properties, duration, ease, autoStart, delay, 4, yoyo);

        // we're using this line for the example to run indefinitely
        //quake.onComplete.addOnce(addQuake);

        // we're using this line for the example to run indefinitely
        quake.onComplete.addOnce(() => {
            if (this.sRover.dif == 4 && this.sRover.hardEart != 5) {
                if (!this.sRover.hardEart) this.sRover.hardEart = 0
                var eart = game.add.sprite(this.sRover.sprite.x, this.sRover.sprite.y, '')
                if (this.sRover.sprite.x > 600 && this.sRover.sprite.y > 600 && this.sRover.sprite.x < 1900 && this.sRover.sprite.y < 1900) {
                    eart.width = (power - power * this.sRover.ImmunEq) * 15
                    eart.height = (power - power * this.sRover.ImmunEq) * 15
                } else {
                    eart.width = (power - power * this.sRover.ImmunEq) * 2
                    eart.height = (power - power * this.sRover.ImmunEq) * 2
                }

                eart.anchor.set(0, 0)
                game.physics.p2.enable(eart)
                eart.body.static = true

                setTimeout(() => { eart.destroy() }, 300)

                this.sRover.hardEart++
            }
            /*
                      
            */
            this.sRover.destruct(power - power * this.sRover.ImmunEq)
            console.log('destruct eqar ' + power - power * this.sRover.ImmunEq);
        })

        // let the earthquake begins
        quake.start();
    },

    contactOnCloud: false,
    contactOnDarkCloud: false,

    _endContactOnDarkCloud: function (body, bodyB, shapeA, shapeB, equation) {
        if (!Helper.isRoverBody(body)) return
        if (this.countDarkCloudOnRover != 0) this.countDarkCloudOnRover--
        game.add.tween(this.rain).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true)

        if (this.countDarkCloudOnRover == 0) {
            this.sRover.unBlockAccumulation()
            this.contactOnCloud = false
            this.contactOnDarkCloud = false
            this.sRover.floor.alpha = 0
        }

        console.log('end contact dark cloud')
        console.log(body.sprite.key)
        console.log('countDarkCloudOnRover= ' + this.countDarkCloudOnRover)
    },
    _hitDarkCloudToRover: function (body, bodyB, shapeA, shapeB, equation) {
        if (!Helper.isRoverBody(body)) return
        this.system.countDarkCloudOnRover++

        if (this.system.isFirstContact) {
            this.system.callbackOnFirstCloudContact()
            this.system.isFirstContact = false
        }

        console.log('hit contact cloud')
        console.log(body.sprite.key)

        this.rover.floor.alpha = 0.4

        this.rover.blockAccumulation()
        this.system.contactOnCloud = true
        this.system.contactOnDarkCloud = true
        if (this.system.rain.alpha != 0) return

        game.add.tween(this.system.rain).to({ alpha: 1 }, 1000, Phaser.Easing.Linear.None, true)
        var rains = this.system.rains
        var index = game.rnd.integerInRange(0, this.system.rains.length - 1)
        if (rains[index].isPlaying) {
            rains[index].restart()
        }
        else {
            rains[index].play()
        }

        console.log('countDarkCloudOnRover= ' + this.system.countDarkCloudOnRover)

    },

    _addTimerRain: function () {
        game.time.events.add(2 * Phaser.Timer.SECOND, () => {
            this._timer = game.time.events.loop(3 * Phaser.Timer.SECOND, () => {
                if (this.currentState != '') {
                    //Определение шансов
                    // var chanceCloud = 35
                    if (this.sRover.dif == 4) {
                        var chanceDarkCloud = 100
                    } else {
                        var chanceDarkCloud = 50 + 10 * this.sRover.dif
                        var chanceClear = 25 - 10 * this.sRover.dif
                    }

                    //Выпад нового состояния
                    var newState = getNewState(chanceDarkCloud, chanceClear)
                    //self.currentState = newState
                    this.currentState = newState

                    switch (newState) {
                        case 'darkCloud':
                            if (this.countClouds > 15 + 10 * this.sRover.dif) break
                            this.createDarkCloud()
                            break;
                        case 'Earthquake':
                            //self.createEarthquake()
                            break;
                        case 'clear':

                            break;
                        default:
                            break;
                    }
                }
            })
        })
    }
}

var getNewState = function (chanceDarkCloud, chanceClear) {
    var magic = this.power = game.rnd.integerInRange(0, 100)

    if (magic < chanceDarkCloud) {
        return 'darkCloud'
    }
    return 'clear'
}
